<?php

session_start();
header('Content-Type: text/plain');

if (empty($_SESSION['login'])) {
	?>
	You are not authenticated.
	Go to `/login.php`, bad boy !
	<?php
}
else {
	?>
	You are authenticated as "<?php echo $_SESSION['login']; ?>" !
	Your password is "<?php echo $_SESSION['passwd']; ?>".
	Kisses.
	<?php
}

