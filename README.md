POC to exploit that affect vuln off alwaysdata (storing PHP sessions in shared
directory)


1. Alice logs on to the authentication page (`src/login.php`).
2. Eve runs the attack (`src/attack.php`) to get a session id.
3. Eve changes her session cookie, and is authenticated as alice
   (`src/is_auth.php`).
